/*
 * Created on: Apr 02, 2022
 *
 * ULID: jlagris
 * Class: IT 168 
 */

package edu.ilstu;

import java.util.Date;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.TitlePaneLayout;

/**
 * IT168 - 007 Lecture Program 4
 *
 * @author John Lagrisola
 *
 */
public class LaptopSalesOrder
{
	// Named variables and constants
	public static final double BASIC_SETUP_AND_SECURITY_CONFIGURATION_PACKAGE = 199.99;
	public static final double REMOVE_VIRUS_OR_MALWARE_PACKAGE = 99.50;
	public static final double INSTALL_PRODUCTIVITY_SOFTWARE_PACKAGE = 299.99;
	public static final double LAPTOP_SALES_TAX = 3.5;
	public static final double SUPPORT_SALES_TAX = 8;
	public static final double SUPPORT_REBATE = 2;

	// Imported java.util.Date;
	// Defined 14 instance variables

	private Date orderDate;
	private double orderPrice;
	private String laptopDescription;
	private double laptopPrice;
	private double laptopTax;
	private double laptopSubtotal;
	private double supportPackagePrice;
	private double supportPackageTax;
	private double supportPackageSubtotal;
	private double supportDollars;
	private double supportRebate;
	private boolean basicSetupAndSecurityConfigurationPackageIndicator;
	private boolean removeVirusOrMalwarePackageIndicator;
	private boolean installProductivitySoftwarePackageIndicator;

	// Define two constructors

	public LaptopSalesOrder()
	{
		orderDate = new Date();
	}

	public LaptopSalesOrder(String description, double laptopPrice)
	{
		this.laptopDescription = description;
		this.laptopPrice = laptopPrice;
		orderDate = new Date();
	}

	// Define setters/getters

	public double getOrderPrice()
	{
		return orderPrice;
	}

	public String getLaptopDescription()
	{
		return laptopDescription;
	}

	public double getSupportDollars()
	{
		return supportDollars;
	}

	public void setLaptopDescription(String laptopDescription)
	{
		this.laptopDescription = laptopDescription;
	}

	// Setter for laptop price and calculations

	public void setLaptopPrice(double laptopPrice)
	{
		this.laptopPrice = laptopPrice;
		double laptopTax = Math.round(laptopPrice * (LAPTOP_SALES_TAX / 100));
		double supportDollars = Math.round(laptopPrice * (SUPPORT_REBATE / 100));
		double laptopSubTotal = laptopPrice + laptopTax;
		double orderPrice = laptopSubTotal + supportPackageSubtotal;
	}

	private void calculateOrderLineItemCosts()
	{
		// basicSetupAndSecurityConfigurationPackageIndicator;
		// removeVirusOrMalwarePackageIndicator;
		// installProductivitySoftwarePackageIndicator;

		// BASIC_SETUP_AND_SECURITY_CONFIGURATION_PACKAGE
		// REMOVE_VIRUS_OR_MALWARE_PACKAGE
		// INSTALL_PRODUCTIVITY_SOFTWARE_PACKAGE

		if (this.basicSetupAndSecurityConfigurationPackageIndicator)
		{
			this.supportPackagePrice += BASIC_SETUP_AND_SECURITY_CONFIGURATION_PACKAGE;
		}
		if (this.removeVirusOrMalwarePackageIndicator)
		{
			this.supportPackagePrice += REMOVE_VIRUS_OR_MALWARE_PACKAGE;
		}
		if (this.installProductivitySoftwarePackageIndicator)
		{
			this.supportPackagePrice += INSTALL_PRODUCTIVITY_SOFTWARE_PACKAGE;
		}
		if (this.supportDollars < this.supportPackagePrice)
		{
			this.supportRebate = supportDollars;
		} else
		{
			this.supportRebate = supportPackagePrice;
		}
		double supportPackageTax = Math.round(this.supportPackagePrice * (SUPPORT_SALES_TAX / 100));
		double supportPackageSubtotal = supportPackagePrice + supportPackageTax + supportRebate;
	}

	public void setBasicSetupAndSecurityConfigurationPackageIndicator(boolean basicSetupAndSecurityConfigurationPackageIndicator){
		this.basicSetupAndSecurityConfigurationPackageIndicator = basicSetupAndSecurityConfigurationPackageIndicator;
		calculateOrderLineItemCosts();
	}

	public void setRemoveVirusOrMalwarePackageIndicator(boolean removeVirusOrMalwarePackageIndicator){
		this.removeVirusOrMalwarePackageIndicator = removeVirusOrMalwarePackageIndicator;
		calculateOrderLineItemCosts();
	}

	public void setInstallProductivitySoftwarePackageIndicator(boolean installProductivitySoftwarePackageIndicator){
		this.installProductivitySoftwarePackageIndicator = installProductivitySoftwarePackageIndicator;
		calculateOrderLineItemCosts();
	}

	// basicSetupAndSecurityConfigurationPackageIndicator;
	// removeVirusOrMalwarePackageIndicator;
	// installProductivitySoftwarePackageIndicator;

	// BASIC_SETUP_AND_SECURITY_CONFIGURATION_PACKAGE
	// REMOVE_VIRUS_OR_MALWARE_PACKAGE
	// INSTALL_PRODUCTIVITY_SOFTWARE_PACKAGE

	// Override the toString method to display the sales order receipt. Copy pasted
	// the program sample
	@Override
	public String toString()
	{
		String packages = "";
		if (this.basicSetupAndSecurityConfigurationPackageIndicator)
		{
			packages = "Basic Setup and Security Configuration";
		}
		if (this.removeVirusOrMalwarePackageIndicator)
		{
			packages = "Remove Virus or Malware";
		}
		if (this.installProductivitySoftwarePackageIndicator)
		{
			packages = "Install Productivity Software";
		}
		return "Congratulations on your purchase!" + "\n--------------------------------------------------"
				+ "\nTERRIFIC TECHNOLOGIES SALES ORDER RECEIPT" + "\nOrder Date: " + this.orderDate
				+ "\n--------------------------------------------------"
				+ "\nItem                                        Amount"
				+ "\n--------------------------------------------------" + "\nLaptop" + " 		\n"
				+ this.laptopDescription + "  \nSale Price:                           " + this.laptopPrice
				+ "  \nSales Tax:                            " + this.laptopTax
				+ "  \nSubtotal:                             " + this.laptopSubtotal + "\nSupport Package" + "\n"
				+ this.basicSetupAndSecurityConfigurationPackageIndicator + "\n"
				+ this.removeVirusOrMalwarePackageIndicator + "\n" + this.installProductivitySoftwarePackageIndicator
				+ "	\nSale Price                           " + this.supportPackagePrice
				+ "	\nSales Tax                            " + this.supportPackageTax
				+ "   \nRebate:                              " + this.supportRebate
				+ "   \nSubtotal:                            " + this.supportPackageSubtotal
				+ "\n--------------------------------------------------" + "\nGrand Total                             "
				+ (this.laptopSubtotal + this.supportPackageSubtotal)
				+ "\n--------------------------------------------------";
	}

	public void setLSOOption(LSOOffer lsoOffer, Boolean variable){
		switch(lsoOffer){
            case BasicSecurityCOnfig:
                this.setBasicSetupAndSecurityConfigurationPackageIndicator(variable);
				break;
            case MalwareRemoval:
				this.setRemoveVirusOrMalwarePackageIndicator(variable);
                break;
            case ProductivitySoftware:
				this.setInstallProductivitySoftwarePackageIndicator(variable);
                break;
            default:
                break;
		}
	}

	enum LSOOffer {

		BasicSecurityCOnfig("Add our basic setup and security configuration package for $199.99 (Y or N)?"),
		MalwareRemoval("Add our virus or malware removal package for $99.50 (Y or N)?"),
		ProductivitySoftware("Add our productivity software installation package for $299.99 (Y or N)? ");
		private String title;

		LSOOffer(String title){
			this.title = title;
		}

		public String getTitle(){
			return title;
		}
	}
}
