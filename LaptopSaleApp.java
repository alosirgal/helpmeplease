/*
 * Created on: Apr 02, 2022
 *
 * ULID: jlagris
 * Class: IT 168 
 */
package edu.ilstu;

import java.util.Scanner;

import edu.ilstu.LaptopSalesOrder.LSOOffer;

/**
 * IT168 - 007 Lecture Program 4
 *
 * @author John Lagrisola
 *
 */
public class LaptopSaleApp
{

	private static final Laptop laptops[];
	static
	{
		Laptop laptop1 = new Laptop("Apple", "Macbook Air", "Intel i7", 16 , 2499);
		Laptop laptop2 = new Laptop("Dell", "Latitude", "Intel i9", 32, 1399);
		Laptop laptop3 = new Laptop("Lenovo", "ThinkPad", "AMD G7", 8 , 1199);
		laptops = new Laptop[] { laptop1, laptop2, laptop3 };

	}

	public static void main(String[] args)
	{

		Scanner scan = new Scanner(System.in);

		int choose = chooseLaptop();

		LaptopSalesOrder LSO = new LaptopSalesOrder(laptops[choose - 1].toString(),laptops[choose - 1].getPrice());

		choosePackageOptions(LSO);
	}

	public static int chooseLaptop()
	{
		Scanner scan = new Scanner(System.in);

		System.out.println("Welcome to Terrific Technologies!");

		System.out.println("\nLet us help you find your next laptop.");
		System.out.println("\nCHOOSE FROM OUR PROLIFIC Laptop INVENTORY . . .");
		System.out.println("--------------------------------------------------");
		System.out.println("Item# Laptop     	    CPU/Ram 	    Price");
		System.out.println("--------------------------------------------------");

		int choose = 0;
		for (int i = 0; i < 3; i++)
		{
			System.out.println(i + 1 + "     " + laptops[i]);
		}
		System.out.print("\nSelect your Laptop by item # (e.g. 1, 2, 3): ");
		choose = scan.nextInt();
		System.out.println("\nYou've selected the " + choose + " . . . a wise choice!");
		return choose;
		// "You've selected the " + choose + ". . . a wise choice!";
	}

	public static void choosePackageOptions(LaptopSalesOrder LSO){

		Scanner scan = new Scanner(System.in);

		String packageChoose;

		System.out.println("\nProtect your purchase with our pre-paid support offerings. (Y/N)");
		String choose = scan.nextLine();
		if(choose.toLowerCase().equals("y")){

			for(LSOOffer lsoOffer: LSOOffer.values()){
				System.out.println(lsoOffer.getTitle());
				packageChoose = scan.nextLine();
				LSO.setLSOOption(lsoOffer, packageChoose.toLowerCase().equals("y"));
			}	
		}else{

			System.out.println("no support offers selected.");
		}

		System.out.println(LSO);
		
	}
	
}