/*
 * Created on: Apr 02, 2022
 *
 * ULID: jlagris
 * Class: IT 168 
 */
package edu.ilstu;

/**
 * IT168 - 007 Lecture Program 4
 *
 * @author John Lagrisola
 *
 */
public class Laptop
{
	private String brand; // 5 Instance Variables
	private String model;
	private String cputype;
	private double ramCapacity;
	private double price;

	public Laptop(String brand, String model, String cputype, double ramCapacity,  double price){
		this.brand = brand;
		this.model = model;
		this.cputype = cputype;
		this.ramCapacity = ramCapacity;
		this.price = price;
	}

	public String getBrand() // constructors for each variable
    {
		return brand;
	}

	public String getModel()
	{
		return model;
	}

	public String getCputype()
	{
		return cputype;
	}

	public double getRamCapacity() // getters and setters for ram capacity
	{
		return ramCapacity;
	}

	public void setRamCapacity(double ramCapacity)
	{
		this.ramCapacity = ramCapacity;
	}

	public double getPrice() // getters and setters for price
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	@Override
	public String toString() // Override toString method
	{
		return "\"" + this.brand + " " + this.model + " " + this.cputype + "\\" +this.getRamCapacity() + " " + this.getPrice();
	}
}
